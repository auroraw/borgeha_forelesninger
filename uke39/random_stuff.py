import random

# Se ppt Terje s 19-20 uke 39
# tilfeldig flyttall fra og med 0 til og uten 1
print(random.random())

# tilfeldig flyttall mellom 0 og arg0
print(random.randint(0,100))


# tilfeldig flyttall mellom arg0 og arg1(med arg2 hopp)
print(random.randrange(100,200,10)) # Akkurat som range
 
 
# Tilfeldig blant en mengde:
print(random.choice('abcdefgh'))
print(random.choice([1,2,'tre']))