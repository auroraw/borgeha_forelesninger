[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.idi.ntnu.no/#https://gitlab.stud.idi.ntnu.no/itgk2020/borgeha_forelesninger) 

# Pythonpod

Denne kan en kjøre opp i nettleseren!
Det er derimot ikke noe dere trenger å tenke på i ITGK, det er slik at jeg kan programmere rett mot disse filene. 
På sikt vil det derimot være noe som dere også kan gjøre. Hvis dere prøver å endre noe her (endre pythonkode) vil det ikke lagres, fordi dere ikke har tilgang.
